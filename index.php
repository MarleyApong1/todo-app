<?php
// Database connection parameters.
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'todolist');
define('DB_HOST', '127.0.0.1');
define('DB_PORT', '3306');

try {
    // Establishing the database connection using PDO
    $dsn = 'mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME;
    $pdo = new PDO($dsn, DB_USER, DB_PASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Connection failed: ' . $e->getMessage());
}

// Handling form actions
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = $_POST['action'] ?? '';
    $id = $_POST['id'] ?? '';
    $title = $_POST['title'] ?? '';

    if ($action === 'new' && !empty($title)) {
        $stmt = $pdo->prepare("INSERT INTO todo (title) VALUES (:title)");
        $stmt->bindParam(':title', $title);
        $stmt->execute();
    } else if ($action === 'delete' && !empty($id)) {
        $stmt = $pdo->prepare("DELETE FROM todo WHERE id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    } else if ($action === 'toggle' && !empty($id)) {
        $stmt = $pdo->prepare("UPDATE todo SET done = 1 - done WHERE id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
}

// Fetching tasks from the database
$taches = [];
$stmt = $pdo->query("SELECT * FROM todo ORDER BY created_at DESC");
$taches = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Todo App</title>
    <link rel="stylesheet" href="./bootstrap-5.0.2/css/bootstrap.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">TodoList</a>
        </div>
    </nav>
    <div class="container mt-5">
        <div class="bg-image h-100">
            <div class="mask d-flex align-items-center h-100">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <form class="d-flex mb-2" method="POST" action="">
                                <input class="form-control me-2" type="text" name="title" placeholder="Add a new task" required>
                                <input type="hidden" name="action" value="new">
                                <button class="btn btn-primary btn-sm" type="submit">Add</button>
                            </form> 
                            <div class="card shadow bg-white">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-borderless mb-0">
                                            <thead>
                                                <tr class="d-flex justify-content-between">
                                                    <th scope="col">TASK</th>
                                                    <th scope="col">ACTIONS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($taches as $tache) : ?>
                                                    <tr class="d-flex justify-content-between <?php echo $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning'; ?>">
                                                        <td><?php echo htmlspecialchars($tache['title']); ?></td>
                                                        <td>
                                                            <form method="POST" action="" class="d-inline">
                                                                <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                                                                <input type="hidden" name="action" value="toggle">
                                                                <button type="submit" class="btn btn-warning btn-sm">Toggle</button>
                                                            </form>
                                                            <form method="POST" action="" class="d-inline">
                                                                <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                                                                <input type="hidden" name="action" value="delete">
                                                                <button type="submit" class="btn btn-danger btn-sm">x</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="./bootstrap-5.0.2/js/bootstrap.js"></script>

</html>
